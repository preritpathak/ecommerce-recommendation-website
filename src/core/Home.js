import React, { useState, useEffect } from "react";
import Layout from "./Layout";
import { getProducts,getRecommendedProducts } from "./apiCore";
import Card from "./Card";
import Search from "./Search";

const Home = () => {
    const [productsBySell, setProductsBySell] = useState([]);
    const [productsByArrival, setProductsByArrival] = useState([]);
    const [recommendedProducts, setRecommendedProducts] = useState([]);
    const [error, setError] = useState(false);

    const loadProductsBySell = () => {
        getProducts("sold").then(data => {
            if (data.error) {
                setError(data.error);
            } else {
                setProductsBySell(data);
            }
        });
    };

    const loadRecommendedProducts = () => {
        getRecommendedProducts().then(data => {
            console.log(data)
            setRecommendedProducts(data.products);
           console.log("HI")
        })
    }

    const loadProductsByArrival = () => {
        getProducts("createdAt").then(data => {
            if (data.error) {
                setError(data.error);
            } else {
                console.log(data)
                setProductsByArrival(data);
            }
        });
    };

    useEffect(() => {
        loadProductsByArrival();
        loadProductsBySell();
        loadRecommendedProducts();
    }, []);

    return (
        <Layout
            title="Home Page"
            description="Dell Shopping Catalogue"
            className="container-fluid"
        >
            <Search />
            <h2 className="mb-4">New Arrivals</h2>
            <div className="row">
                {productsByArrival.map((product, i) => (
                    <div key={i} className="col-4 mb-3">
                        <Card product={product} />
                    </div>
                ))}
            </div>

            <h2 className="mb-4">Best Sellers</h2>
            <div className="row">
                {productsBySell.map((product, i) => (
                    <div key={i} className="col-4 mb-3">
                        <Card product={product} />
                    </div>
                ))}
            </div>
            <h2 className="mb-4">Recommended Products</h2>
            <div className="row">
                {recommendedProducts.map((product, i) => (
                    <div key={i} className="col-4 mb-3">
                        {/* <Card product={product} /> */}
                       {product.model}
                    </div>
                ))}
            </div>
         </Layout>
    );
};

export default Home;
