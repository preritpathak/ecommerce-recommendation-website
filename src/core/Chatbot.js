import React, { Component } from 'react';
import Layout from "./Layout";
import axios from 'axios'
import PropTypes from 'prop-types';
import ChatBot from 'react-simple-chatbot';
import { randomBytes } from 'crypto';
import { CLIENT_RENEG_LIMIT } from 'tls';
import { getProducts,getRecommendedProducts } from "./apiCore";

class Review extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      role: '',
      ram: '',
      battery:'',
      imp:'',
      size:'',
      budget: '',
    };
  }


  componentWillMount() {
    const { steps } = this.props;
    const { name, role, battery,imp,size, ram, budget } = steps;
    var processor=0;

    this.setState({ name, role, ram, battery,imp,size, budget  });
    if(battery.value="Frequently"){
      battery.value=3
    }
    else if(battery.value="Ocassionally"){
      battery.value=2
    }
    else{
      battery.value=1
    }
    if(imp.value="Touchscreen"){
      imp.value=1
    }
    else{
      imp.value=0
    }
    if(ram.value="Gaming"){
      ram.value=16
      processor=9
    }
    else if(ram.value="Programming"){
      ram.value=4
      processor=7
    }
    else{
      ram.value=8
      processor=5
    }
    if(size.value="Small"){
      size.value=33
    }
    else if(size.value="Medium"){
      size.value=35
    }
    else{
      size.value=39
    }
    
    var data= {"name":name.value,"role":role.value,"ram":ram.value, "battery":battery.value,"touch":imp.value,"size":size.value,"budget":budget.value,"processor":processor}
    console.log(data)
    axios.post('http://localhost:8000/api/data', {
      data:data
    }).then(res => {
      console.log(res)
    }).catch(err => {
      console.log(err.message)
    })
  }

  render() {
    const { name, role, ram, battery,imp,size, budget  } = this.state;
    return (
      <div style={{ width: '100%' }}>
        <h3>Summary</h3>
        <table>
          <tbody>
            <tr>
              <td>Name</td>
              <td>{name.value}</td>
            </tr>
            <tr>
              <td>Role</td>
              <td>{role.value}</td>
            </tr>
            <tr>
              <td>Usage</td>
              <td>{ram.value}</td>
            </tr><tr>
              <td>Battery</td>
              <td>{battery.value}</td>
            </tr><tr>
              <td>Importance</td>
              <td>{imp.value}</td>
            </tr> 
            <tr>
              <td>Size</td>
              <td>{size.value}</td>
            </tr>
            <tr>
              <td>Budget</td>
              <td>{budget.value}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
  
}

class SimpleForm extends Component {
  render() {
    return (
      <ChatBot
      speechSynthesis={{ enable: true, lang: 'en' }}
      recognitionEnable={true}
      steps={[
          {
            id: '1',
            message: 'What is your name?',
            trigger: 'name',
          },
          {
            id: 'name',
            user: true,
            trigger: '3',
          },
          {
            id: '3',
            message: 'Hi {previousValue}! What will be your main ram for this laptop?',
            trigger: 'role',
          },
          {
            id: 'role',
            options: [
              { value: 'personal', label: 'Personal', trigger: '4' },
              { value: 'professional', label: 'Professional', trigger: '4' },
              { value: 'others', label: 'Others', trigger: '4' },
            ],
          },
          {
            id:'4',
            message: 'What will you ram the laptop mostly for?',
            trigger:'ram',
          },
          {
            id:'ram',
            options: [
              { value: 'gaming', label: 'Gaming', trigger: '5' },
              { value: 'programming', label: 'Programming', trigger: '5' },
              { value: 'videoediting', label: 'Video Editing', trigger: '5' },
            ],
          },
          {
            id:'5',
            message: 'How often do you plan to carry your laptop?',
            trigger:'battery',
          },
          {
            id:'battery',
            options: [
              { value: 'frequently', label: 'Frequently', trigger: '6' },
              { value: 'ocassionally', label: 'Ocasionally', trigger: '6' },
              { value: 'notatall', label: 'Not at all', trigger: '6' }
            ],
          },
          {
            id:'6',
            message: 'What would you choose among the following?',
            trigger:'imp',
          },
          {
            id:'imp',
            options: [
              { value: 'touchscreen', label: 'Touchscreen', trigger: '7' },
              { value: 'nontouchscreen', label: 'Non-Touchscreen', trigger: '7' },
            ],
          },
          {
            id:'7',
            message: 'Is there a specific screen size that you will prefer',
            trigger:'size',
          },
          {
            id:'size',
            options: [
              { value: 'small', label: 'Small', trigger: '9' },
              { value: 'medium', label: 'Medium', trigger: '9' },
              { value: 'large', label: 'Large', trigger: '9' },
            ],
          },
          {
            id: '9',
            message: 'What is your budget? (please enter without commas)',
            trigger: 'budget',
          },
          {
            id: 'budget',
            user: true,
            trigger: '8',
            validator: (value) => {
              if (isNaN(value)) {
                return 'Value must be a number';
              } else if (value < 0 || value == 0) {
                return 'Please specify a budget';
              } 

              return true;
            },
          },
          {
            id: '8',
            message: 'Great! Check out your summary',
            trigger: 'review',
          },
          {
            id: 'review',
            component: <Review />,
            asMessage: true,
            end: true,
          },
          {
            id: 'update',
            message: 'Would you like to update some field?',
            trigger: 'update-question',
          },
          {
            id: 'update-question',
            options: [
              { value: 'yes', label: 'Yes', trigger: 'update-yes' },
              { value: 'no', label: 'No', trigger: 'end-message' },
            ],
          },
          {
            id: 'update-yes',
            message: 'What field would you like to update?',
            trigger: 'update-fields',
          },
          {
            id: 'update-fields',
            options: [
              { value: 'name', label: 'Name', trigger: 'update-name' },
              { value: 'role', label: 'role', trigger: 'update-role' },
              { value: 'budget', label: 'Budget', trigger: 'update-budget' },
            ],
          },
          {
            id: 'update-name',
            update: 'name',
            trigger: '7',
          },
          {
            id: 'update-role',
            update: 'role',
            trigger: '7',
          },
          {
            id: 'update-budget',
            update: 'budget',
            trigger: '7',
          },
          {
            id: 'end-message',
            message: 'Thanks! Your data was submitted successfully!',
            end: true,
          },
        ]}
      />
          );
      }
}

// const loadRecommendedProducts = () => {
//   getRecommendedProducts().then(data => {
//       console.log(data.products)
//       setRecommendedProducts(data.products);
     
//   })
// }


export default SimpleForm;